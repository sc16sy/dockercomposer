module.exports = {
  // module: {
  //   rules: [
  //     {
  //       test: /\.scss$/,
  //       use: ["vue-style-loader", "css-loader", "sass-loader"]
  //     }
  //   ]
  // },
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].title = 'Docker Composer';
      return args;
    });
  },
};
