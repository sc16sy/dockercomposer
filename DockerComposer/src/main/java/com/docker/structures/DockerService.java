package com.docker.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DockerService {
    private String image;
    private List<String> ports = new ArrayList<>();
    private Map<String, String> environment = new HashMap<>();


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getPorts() {
        return ports;
    }

    public void setPorts(List<String> ports) {
        this.ports = ports;
    }

    public List<String> getEnvironment() {
        List<String> env = new ArrayList<>();
        environment.forEach((key, value) -> env.add(key + "=" + value));

        return env;
    }

    public void setEnvironment(Map<String, String> environment) {
        this.environment = environment;
    }
}
