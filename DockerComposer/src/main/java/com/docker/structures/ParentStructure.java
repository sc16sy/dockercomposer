package com.docker.structures;

public class ParentStructure {

    private boolean success = true;

    public boolean isSuccess() {
        return success;
    }

    protected void setSuccess(boolean success) {
        this.success = success;
    }
}
