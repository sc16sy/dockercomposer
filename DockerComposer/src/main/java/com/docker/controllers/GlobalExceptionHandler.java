package com.docker.controllers;

import com.docker.structures.Error;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({Exception.class})
    public final ResponseEntity<Error> handleException(Exception ex) {
        ex.printStackTrace();
        Throwable mainEx = ex;
        while (mainEx.getCause() != null) mainEx = mainEx.getCause();

        return new ResponseEntity<>(new Error(mainEx.getMessage()), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
