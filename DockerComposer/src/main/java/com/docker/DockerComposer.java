package com.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerComposer {

    public static void main(String[] args) {
        SpringApplication.run(DockerComposer.class, args);
    }

}
