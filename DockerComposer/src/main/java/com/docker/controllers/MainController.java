package com.docker.controllers;

import com.docker.core.Compose;
import com.docker.structures.DockerSettings;
import com.docker.structures.ParentStructure;
import com.docker.structures.YamlResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @CrossOrigin
    @PostMapping("/create")
    public ParentStructure create(@RequestBody DockerSettings data) throws JsonProcessingException {
        return new YamlResponse(new Compose(data).toString());
    }
}
