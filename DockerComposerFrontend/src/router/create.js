import Create from '@/views/Create.vue';

export default [
  {
    path: '/create',
    name: 'Create',
    component: Create,
  },
];
