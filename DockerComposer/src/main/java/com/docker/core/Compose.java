package com.docker.core;

import com.docker.structures.DockerSettings;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;

public class Compose {
    private final String yamlData;

    public Compose(DockerSettings settings) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory().disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER));
        yamlData = mapper.writeValueAsString(settings);
    }

    @Override
    public String toString() {
        return yamlData;
    }
}
