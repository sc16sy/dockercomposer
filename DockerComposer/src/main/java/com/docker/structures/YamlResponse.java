package com.docker.structures;

public class YamlResponse extends ParentStructure {
    private final String compose;

    public YamlResponse(String compose) {
        this.compose = compose;
    }

    public String getCompose() {
        return compose;
    }
}
