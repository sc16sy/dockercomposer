import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './home';
import Create from './create';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [...Home, ...Create],
});
