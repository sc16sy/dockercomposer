package com.docker.structures;

public class Error extends ParentStructure {
    private final String errorMessage;

    public Error(String errorMessage) {
        setSuccess(false);
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
