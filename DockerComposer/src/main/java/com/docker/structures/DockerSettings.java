package com.docker.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DockerSettings {
    private final String version = "3.7";
    private Map<String, DockerService> services = new HashMap<>();

    public String getVersion() {
        return version;
    }

    public Map<String, DockerService> getServices() {
        return services;
    }

    public void setServices(Map<String, DockerService> services) {
        this.services = services;
    }
}
